# README

Die eigentlichen Dateien befinden sich im [wiki](https://gitlab.math.ethz.ch/support-articles/mfa-howto/-/wikis/home) des Projekts.
Diese können geklont werden mit dem Befehl:

```bash
# SSH:
git clone git@gitlab.math.ethz.ch:support-articles/mfa-howto.wiki.git

# HTTPS
git clone https://gitlab.math.ethz.ch/support-articles/mfa-howto.wiki.git
```
